# yd-ably-coding-test
- 프론트엔드 지원자 김예닮의 코딩 테스트 프로젝트입니다.

### 로컬 서버 실행
```
yarn serve
```

### 사용기술
- vue 3(composition api)
- axios
- typescript
- scss

### 구현 스펙
- 로그인 페이지 (`/`로 접속할 수 있습니다.)
- 비밀번호 재설정 페이지 (`/resetPassword`로 접속할 수 있습니다.)
- 회원정보 페이지 (`/userInfo`로 접속할 수 있습니다.)
