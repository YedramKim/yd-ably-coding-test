import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
	{
		path: '/',
		name: 'pageLogin',
		component: () => import(/* webpackChunkName: "page-login" */ '../views/page-login.vue'),
	},
	{
		path: '/userInfo',
		name: 'userInfo',
		component: () => import(/* webpackChunkName: "page-login" */ '../views/user-info.vue'),
	},
	{
		path: '/resetPassword',
		component: () => import(/* webpackChunkName: "reset-password" */ '../views/reset-password.vue'),
		children: [
			{
				path: '',
				name: 'resetPassword',
				redirect: { name: 'requestAuthCode' },
			},
			{
				path: 'requestAuthCode',
				name: 'requestAuthCode',
				component: () =>
					import(
						/* webpackChunkName: "reset-password-request-code" */ '../views/reset-password-request-code.vue'
					),
			},
			{
				path: 'validateAuthCode',
				name: 'validateAuthCode',
				component: () =>
					import(
						/* webpackChunkName: "reset-password-validate-code" */ '../views/reset-password-validate-code.vue'
					),
			},
			{
				path: 'passwordForm',
				name: 'passwordForm',
				component: () =>
					import(/* webpackChunkName: "reset-password-form" */ '../views/reset-password-form.vue'),
			},
		],
	},
];

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes,
});

export default router;
