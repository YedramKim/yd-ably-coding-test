import { get } from './base';

interface userInfoData {
	name: string;
	email: string;
	profileImage: string;
	lastConnectedAt: number;
}

export const getUserInfo = (): Promise<userInfoData> => get('/api/user', {}, { useAuth: true });
