import { post } from './base';

interface LoginPayload {
	email: string;
	password: string;
}

interface LoginData {
	accessToken: string;
}

export const login = (payload: LoginPayload): Promise<LoginData> => post('/api/login', payload);

export const logout = (): Promise<void> => post('/api/logout', {}, { useAuth: true });
