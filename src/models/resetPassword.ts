import { get, post, patch } from './base';

interface ResetPasswordData {
	issueToken: string;
	remainMillisecond: number;
}

interface ValidateAuthCodePayload {
	email: string;
	authCode: string;
	issueToken: string;
}

interface ValidateAuthCodeData {
	confirmToken: string;
}

interface ResetPasswordPayload {
	email: string;
	confirmToken: string;
	newPassword: string;
	newPasswordConfirm: string;
}

export const requestAuthCode = (email: string): Promise<ResetPasswordData> =>
	get('/api/reset-password', {
		email,
	});

export const validateAuthCode = (payload: ValidateAuthCodePayload): Promise<ValidateAuthCodeData> =>
	post('/api/reset-password', payload);

export const resetPasswowrd = (payload: ResetPasswordPayload): Promise<void> =>
	patch('/api/reset-password', payload);
