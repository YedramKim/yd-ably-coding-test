import axios from 'axios';

import { network as networkContext } from '../contexts';

axios.defaults.baseURL = 'https://ably-frontend-interview-server.vercel.app';
axios.defaults.headers.common['Content-Type'] = 'application/json';

type HTTPMethod = 'GET' | 'POST' | 'PATCH';

type FetchConfig = Partial<{
	useAuth: boolean;
}>;

const getFetchHeaders = (config: FetchConfig = {}) => {
	const { useAuth = false } = config;
	const { accessToken } = networkContext.useAccessToken();

	return {
		Authorization: useAuth ? `Bearer ${accessToken.value}` : undefined,
	};
};

interface FetchOption {
	method: HTTPMethod;
	url: string;
	data: any;
	config: FetchConfig;
}

const fetch = async <T>({ method, url, data, config }: FetchOption) => {
	try {
		const headers = getFetchHeaders(config);

		const dataPropertyName = method === 'GET' ? 'params' : 'data';
		const { data: result } = await axios.request<T>({
			method,
			url,
			headers,
			[dataPropertyName]: data,
		});

		return result;
	} catch (error) {
		const { setServerErrorMessage } = networkContext.useServerErrorMessage();
		setServerErrorMessage(error.response.data.error.message);
		throw error;
	}
};

export const get = <T>(url: string, data?: any, config?: FetchConfig): Promise<T> =>
	fetch({ method: 'GET', url, data, config });

export const post = <T>(url: string, data?: any, config?: FetchConfig): Promise<T> =>
	fetch({ method: 'POST', url, data, config });

export const patch = <T>(url: string, data?: any, config?: FetchConfig): Promise<T> =>
	fetch({ method: 'PATCH', url, data, config });
