import { ref } from 'vue';

import { storageProxy } from '../utils';

const accessToken = ref(storageProxy.accessToken as string);

let removeServerErrorMessageTimer: number = null;

const TIME_TO_REMOVE_SERVER_ERROR_MESSAGE = 5000;

const setAccessToken = (token: string) => {
	accessToken.value = token;
	storageProxy.accessToken = token;
};

const serverErrorMessage = ref('');

const removeServerErrorMessage = () => {
	clearTimeout(removeServerErrorMessageTimer);
	serverErrorMessage.value = '';
};

const setServerErrorMessage = (message: string) => {
	serverErrorMessage.value = message;

	removeServerErrorMessageTimer = setTimeout(() => {
		removeServerErrorMessage();
	}, TIME_TO_REMOVE_SERVER_ERROR_MESSAGE);
};

export const useAccessToken = () => ({
	accessToken,
	setAccessToken,
});

export const useServerErrorMessage = () => ({
	serverErrorMessage,
	setServerErrorMessage,
	removeServerErrorMessage,
});
