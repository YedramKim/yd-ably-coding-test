import { ref } from 'vue';

import { resetPassword as resetPasswordModels } from '@/models';

const userEmail = ref('');
const expireTime = ref(0);
const issueToken = ref('');
const confirmToken = ref('');
interface IssueTokenData {
	email: string;
	issueToken: string;
	remainMillisecond: number;
}

const setIssueToken = (data: IssueTokenData) => {
	userEmail.value = data.email;
	issueToken.value = data.issueToken;
	expireTime.value = Date.now() + data.remainMillisecond;
};

const setConfirmToken = (token: string) => {
	confirmToken.value = token;
};

const resetContext = () => {
	userEmail.value = '';
	expireTime.value = 0;
	issueToken.value = '';
	confirmToken.value = '';
};

const requestAuthCode = async (email: string) => {
	const { issueToken, remainMillisecond } = await resetPasswordModels.requestAuthCode(email);

	setIssueToken({ email, issueToken, remainMillisecond });
};

const validateAuthCode = async (code: string) => {
	const result = await resetPasswordModels.validateAuthCode({
		email: userEmail.value,
		authCode: code,
		issueToken: issueToken.value,
	});

	setConfirmToken(result.confirmToken);
};

const resetPassword = async (password: string, passwordConfirm: string) => {
	await resetPasswordModels.resetPasswowrd({
		email: userEmail.value,
		confirmToken: confirmToken.value,
		newPassword: password,
		newPasswordConfirm: passwordConfirm,
	});

	resetContext();
};

export const useRequestAuthCode = () => ({
	requestAuthCode,
});

export const useExpireTime = () => ({
	expireTime,
});

export const useValidateAuthCode = () => ({
	validateAuthCode,
});

export const useResetPassword = () => ({
	resetPassword,
});
