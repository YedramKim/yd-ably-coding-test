import { useAccessToken } from './network';
import { login as loginModels } from '../models';

const login = async (email: string, password: string) => {
	const { setAccessToken } = useAccessToken();

	const { accessToken } = await loginModels.login({ email, password });
	setAccessToken(accessToken);
};

const logout = async () => {
	const { setAccessToken } = useAccessToken();

	await loginModels.logout();
	setAccessToken(null);
};

export const useLogin = () => ({
	login,
});

export const useLogout = () => ({
	logout,
});
