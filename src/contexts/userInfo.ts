import { computed, Ref, ref } from 'vue';

import { userInfo } from '../models';

interface UserInfo {
	name: string;
	email: string;
	profileImage: string;
	lastConnectedAt: number;
}

const name = ref('');
const email = ref('');
const profileImage = ref('');
const lastConnectedAt = ref(null) as Ref<Date>;

const isLoaded = computed(() => profileImage.value !== '');

const setUserInfo = (userInfo: UserInfo) => {
	name.value = userInfo.name;
	email.value = userInfo.email;
	profileImage.value = userInfo.profileImage;
	lastConnectedAt.value = new Date(userInfo.lastConnectedAt);
};

const getUserInfo = async () => {
	const result = await userInfo.getUserInfo();
	setUserInfo(result);
};

export const useUserInfo = () => ({ name, email, profileImage, lastConnectedAt, isLoaded });

export const useGetUserInfo = () => ({ getUserInfo });
