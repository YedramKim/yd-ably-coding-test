const storageProxy = new Proxy(localStorage, {
	get(storage, key) {
		const value = storage.getItem(key as string);

		return value ? JSON.parse(value) : null;
	},

	set(storage, key, value) {
		if (value === null || value === undefined) {
			storage.removeItem('');
		} else {
			storage.setItem(key as string, JSON.stringify(value));
		}

		return true;
	},
});

export default storageProxy;
