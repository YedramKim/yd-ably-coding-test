export default (password: string, passwordConfirm: string): boolean => {
	if (password === '') return false;
	if (passwordConfirm === '') return false;
	return password === passwordConfirm;
};
