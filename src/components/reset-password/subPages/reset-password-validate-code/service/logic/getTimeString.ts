export default (milliSecond: number): string => {
	const ONE_SECOND = 1000;
	const ONE_MINUTE = 60;

	const second = Math.floor(milliSecond / ONE_SECOND);
	const secondString = (second % 60).toString().padStart(2, '0');
	const minute = Math.floor(second / ONE_MINUTE);

	return `${minute}:${secondString}`;
};
