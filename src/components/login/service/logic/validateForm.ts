import { validateEmail } from '@/logic';

export default (email: string, password: string) =>
	email !== '' && password !== '' && validateEmail(email);
